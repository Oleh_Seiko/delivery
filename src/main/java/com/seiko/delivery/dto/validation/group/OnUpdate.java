package com.seiko.delivery.dto.validation.group;

import javax.validation.groups.Default;

public interface OnUpdate extends Default {
}
