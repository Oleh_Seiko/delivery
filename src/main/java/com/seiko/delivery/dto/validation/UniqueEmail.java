package com.seiko.delivery.dto.validation;

import com.seiko.delivery.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueEmail.UniqueEmailValidator.class)
public @interface UniqueEmail {


    String message() default "{com.seiko.delivery.dto.validation.UniqueEmail}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

        @Autowired
        public UserService userService;

        @Override
        public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
            return !userService.isUserExistsWithEmail(email);
        }
    }
}
