package com.seiko.delivery.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.seiko.delivery.dto.validation.FieldMatch;
import com.seiko.delivery.dto.validation.UniqueEmail;
import com.seiko.delivery.dto.validation.group.OnRegister;
import com.seiko.delivery.dto.validation.group.OnSignIn;
import com.seiko.delivery.dto.validation.group.OnUpdate;
import com.seiko.delivery.model.Order;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;
import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Data
@FieldMatch(first = "password", second = "repeatPassword", groups = OnRegister.class)
public class UserDto {

    @JsonProperty(access = READ_ONLY)
    private Long id;

    @NotBlank(groups = OnRegister.class, message = "{name.blank}")
    @Null(groups = OnSignIn.class, message = "{name not null}")
    private String name;

    @NotBlank(groups = OnRegister.class, message = "{surname.blank}")
    @Null(groups = OnSignIn.class, message = "{surname not null}")
    private String surname;

    @Null(groups = OnUpdate.class, message = "{password not null}")
    @NotBlank(groups = {OnRegister.class, OnSignIn.class}, message = "{password.blank}")
    private String password;

    @JsonProperty(access = WRITE_ONLY)
    @Null(groups = {OnUpdate.class, OnSignIn.class}, message = "{repeatPassword.not.null}")
    @NotBlank(groups = OnRegister.class, message = "{repeatPassword.blank}")
    private String repeatPassword;

    @ApiModelProperty(notes = "Unique user's email")
    @NotBlank(groups = {OnRegister.class, OnSignIn.class}, message = "{email blank}")
    @Email(message = "{email.wrong.format}")
    @UniqueEmail(groups = {OnRegister.class, OnUpdate.class})
    private String email;


    private List<Order> orders;

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", password='" + password + '\'' +
                ", repeatPassword='" + repeatPassword + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
