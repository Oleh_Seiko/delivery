package com.seiko.delivery.dto;

import com.seiko.delivery.dto.validation.group.OnRegister;
import com.seiko.delivery.dto.validation.group.OnUpdate;
import com.seiko.delivery.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    @Null(groups = OnRegister.class)
    @NotNull(groups = OnUpdate.class)
    private Long id;

    @NotBlank(groups = OnRegister.class)
    private String cityStart;

    @NotBlank(groups = OnRegister.class)
    private String cityFinish;

    private int weight;

    private int km;
    private int sum;

    @NotBlank(groups = OnRegister.class)
    private String date;
    private User user;
}
