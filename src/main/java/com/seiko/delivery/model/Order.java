package com.seiko.delivery.model;

import lombok.Data;
import lombok.ToString;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;

@Data
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "city_start")
    private String cityStart;

    @Column(name = "city_finish")
    private String cityFinish;
    private int weight;
    private int km;
    private int sum;
    private String date;
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private User user;

}
