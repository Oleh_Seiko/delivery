package com.seiko.delivery.exception.order;

public class UnableToGetOrderException extends RuntimeException{

    public UnableToGetOrderException(String message, Throwable cause) {
        super(message, cause);
    }
}
