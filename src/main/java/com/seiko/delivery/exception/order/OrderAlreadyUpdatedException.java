package com.seiko.delivery.exception.order;

public class OrderAlreadyUpdatedException extends RuntimeException{

    public OrderAlreadyUpdatedException(String message) {
        super(message);
    }
}
