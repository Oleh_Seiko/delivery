package com.seiko.delivery.exception.order;

public class OrderAlreadyDeletedException extends RuntimeException{

    public OrderAlreadyDeletedException(String message) {
        super(message);
    }
}
