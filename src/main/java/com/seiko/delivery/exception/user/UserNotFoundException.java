package com.seiko.delivery.exception.user;

import com.seiko.delivery.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException(String message) {
        super(message);
    }
}
