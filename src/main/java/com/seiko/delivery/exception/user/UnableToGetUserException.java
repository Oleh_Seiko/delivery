package com.seiko.delivery.exception.user;

import com.seiko.delivery.exception.AbstractException;
import com.seiko.delivery.model.enums.ErrorCode;
import com.seiko.delivery.model.enums.ErrorType;

public class UnableToGetUserException extends AbstractException {

    public UnableToGetUserException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public ErrorCode getErrorCode(){return ErrorCode.APPLICATION_ERROR_CODE;}

    @Override
    public ErrorType getErrorType(){return  ErrorType.FATAL_ERROR_CODE;}
}
