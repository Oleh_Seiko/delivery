package com.seiko.delivery.controller;

import com.seiko.delivery.api.AuthApi;
import com.seiko.delivery.controller.assembler.UserModelAssembler;
import com.seiko.delivery.controller.model.UserModel;
import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.enums.Role;
import com.seiko.delivery.services.auth.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AuthController implements AuthApi {

    private final AuthService authService;
    private final UserModelAssembler modelAssembler;

    @Override
    public UserModel signIn(UserDto inUserDto){
        log.info("signIn: SignIn in user with email {}", inUserDto.getEmail());
        UserDto outUserDto = authService.signIn(inUserDto);
        log.info("signIn: outUserDto", outUserDto.getEmail());
        return modelAssembler.toModel(outUserDto);
    }

    @Override
    public UserModel signUp(UserDto inUserDto){
        log.info("signUp: Registering in user with email {}", inUserDto.getEmail());
        UserDto outUserDto = authService.signUp(inUserDto, Role.ROLE_USER);
        log.info("signUp: outUserDto", outUserDto);
        return modelAssembler.toModel(outUserDto);

    }

    @Override
    public ResponseEntity<Void> signOut(){
        log.info("signOut: enter");
        authService.signOut();
        return ResponseEntity.noContent().build();
    }

}
