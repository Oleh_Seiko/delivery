package com.seiko.delivery.controller;

import com.seiko.delivery.api.AdminApi;
import com.seiko.delivery.controller.assembler.UserModelAssembler;
import com.seiko.delivery.controller.model.UserModel;
import com.seiko.delivery.dto.OrderDto;
import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import com.seiko.delivery.services.OrderService;
import com.seiko.delivery.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AdminController implements AdminApi {

    private final UserService userService;
    private final UserModelAssembler modelAssembler;
    private final OrderService orderService;

    @Override
    public CollectionModel<UserModel> getAllUsers() {
        log.info("getAllUsers(): method is called in AdminController");
        List<UserDto> allUsers = userService.getAllUsers();
        log.info("getAllUsers(): get allUsers in controller");
        CollectionModel<UserModel> userModels = modelAssembler.toCollectionModel(allUsers);
        log.info("getAllUsers(): userModels", userModels);
        return userModels;
    }

    @Override
    public UserModel getUserById(Long id) {
        log.info("getUserById: started");
        UserDto userById = userService.getUserById(id);
        log.info("getUserById: get user {}", userById);
        return modelAssembler.toModel(userById);
    }

    @Override
    public ResponseEntity<Void> deleteUserById(User user, Long id) {
        log.info("deleteUserById: start");
        userService.deleteUserById(user, id);
        return ResponseEntity.ok().build();
    }

    @Override
    public List<OrderDto> getAllOrders() {
        log.info("getAllOrders: start");
        List<OrderDto> allOrders = orderService.getAllOrders();
        log.info("getAllOrders: get allOrders {}", allOrders);
        return allOrders;
    }

    @Override
    public OrderDto getOrderById(Long id) {
        log.info("getOrderById: start");
        OrderDto orderDto = orderService.getOrderById(id);
        log.info("getOrderById: with id {}", orderDto);
        return orderDto;
    }
}
