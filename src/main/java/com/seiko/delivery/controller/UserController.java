package com.seiko.delivery.controller;

import com.seiko.delivery.api.UserApi;
import com.seiko.delivery.controller.assembler.UserModelAssembler;
import com.seiko.delivery.controller.model.UserModel;
import com.seiko.delivery.dto.OrderDto;
import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import com.seiko.delivery.services.mapping.MappingService;
import com.seiko.delivery.services.OrderService;
import com.seiko.delivery.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;
    private final MappingService mappingService;
    private final UserModelAssembler modelAssembler;
    private final OrderService orderService;


    @Override
    public UserModel getCurrentUser(User user) {
        log.info("getUserById: getUser with id {}", user.getId());
        UserDto userDto = mappingService.mapFromUserToUserDto(user);
        log.info("getUserById: userDto", userDto);
        return modelAssembler.toModel(userDto);
    }

    @Override
    public UserModel updateUser(UserDto userDto) {
        log.info("updateUser: updateUser create");
        UserDto user = userService.updateUser(userDto);
        log.info("updateUser: get user", user);
        return modelAssembler.toModel(user);
    }

    @Override
    public ResponseEntity<Void> deleteUser(User user) {
        log.info("deleteUser: create method");
        userService.deleteUser(user);
        return ResponseEntity.noContent().build();
    }

    @Override
    public Order createOrder(Order order, User user) {
        log.info("createOrder : userId {}, and order {}", user.getId(), order);
        order.setUser(user);
        Order order1 = orderService.create(order);
        log.info("createOrder : created order {}", order1);
        return order1;
    }

    @Override
    public OrderDto getOrderById(Long id) {
        log.info("getOrderById: start");
        OrderDto orderDto = orderService.getOrderById(id);
        log.info("getOrderById: with id {}", orderDto);
        return orderDto;
    }

    @Override
    public List<OrderDto> getOrdersByUser(User user) {
        log.info("getOrdersByUser(User user, Long id): start");
        List<OrderDto> allOrders = orderService.getOrdersByUser(user);
        log.info("getOrdersByUser(User user, Long id): with id {}", allOrders);
        return allOrders;
    }

    @Override
    public Order updateOrder(Order order) {
        log.info("updateOrder: start");
        Order updateOrder = orderService.update(order);
        log.info("updateOrder: ", updateOrder);
        return updateOrder;
    }

    @Override
    public ResponseEntity<Void> deleteOrder(User user, Long id) {
        log.info("delete: start");
        orderService.delete(user, id);
        log.info("delete is over :)");
        return ResponseEntity.ok().build();
    }

}
