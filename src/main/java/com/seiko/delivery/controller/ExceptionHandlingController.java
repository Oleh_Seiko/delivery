package com.seiko.delivery.controller;

import com.seiko.delivery.exception.AbstractException;
import com.seiko.delivery.exception.order.OrderAlreadyDeletedException;
import com.seiko.delivery.exception.order.OrderAlreadyUpdatedException;
import com.seiko.delivery.exception.order.UnableToGetOrderException;
import com.seiko.delivery.model.Error;
import com.seiko.delivery.model.enums.ErrorCode;
import com.seiko.delivery.model.enums.ErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.HandlerMethod;

import java.time.LocalDateTime;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(AbstractException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleAbstractException(AbstractException ex, HandlerMethod hm){
        log.error("handleAbstractException: exception {}, method {}", ex, hm.getMethod().getName());
        return new Error(ex.getMessage(), ex.getErrorCode(), ex.getErrorType(), LocalDateTime.now());
    }

    @ExceptionHandler(OrderAlreadyUpdatedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleOrderAlreadyUpdatedException(OrderAlreadyUpdatedException ex, HandlerMethod hm){
        log.error("handleOrderAlreadyUpdatedException: exception{}, method {}", ex, hm.getMethod().getName() );
        return new Error(ex.getMessage(), ErrorCode.APPLICATION_ERROR_CODE, ErrorType.FATAL_ERROR_CODE, LocalDateTime.now());
    }

    @ExceptionHandler(OrderAlreadyDeletedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleMethodNotCreateOrder(OrderAlreadyDeletedException ex, HandlerMethod hm){
        log.error("handleMethodNotCreateOrder: exception{}, method {}", ex, hm.getMethod().getName());
        return new Error(ex.getMessage(), ErrorCode.APPLICATION_ERROR_CODE, ErrorType.FATAL_ERROR_CODE,LocalDateTime.now());
    }

    @ExceptionHandler(UnableToGetOrderException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleUnableToGetOrderException(UnableToGetOrderException ex, HandlerMethod hm){
        log.error("handleUnableToGetOrderException: exception {}, method {}", ex, hm.getMethod().getName());
        return new Error(ex.getMessage(), ErrorCode.APPLICATION_ERROR_CODE, ErrorType.FATAL_ERROR_CODE,LocalDateTime.now());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleAllException(Exception ex, HandlerMethod hm){
        log.error("handleAllException: exception {} method {}", ex, hm.getMethod().getName());
        return new Error(ex.getMessage(), ErrorCode.APPLICATION_ERROR_CODE, ErrorType.FATAL_ERROR_CODE, LocalDateTime.now());
    }

}
