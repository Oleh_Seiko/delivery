package com.seiko.delivery.api;

import com.seiko.delivery.controller.model.UserModel;
import com.seiko.delivery.dto.OrderDto;
import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.dto.validation.group.OnUpdate;
import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.parameters.P;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "User management REST API")
@RequestMapping(value = "/api/v1/user")
public interface UserApi {

    @ApiOperation("Get user API")
    @ApiResponse(code = 200, message = "OK", response = UserModel.class)
    @GetMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    UserModel getCurrentUser(@AuthenticationPrincipal User user);

    @ApiOperation("Update user Api")
    @ApiResponse(code = 200, message = "OK", response = UserModel.class)
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    UserModel updateUser(@RequestBody @Validated(OnUpdate.class) UserDto userDto);

    @ApiOperation("Delete user API")
    @ApiResponse(code = 204, message = "No content")
    @DeleteMapping()
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<Void> deleteUser(@AuthenticationPrincipal User user);

    @ApiOperation("Create userOrders API")
    @ApiResponse(code = 201, message = "Created", response = Order.class)
    @PostMapping("/orders")
    @ResponseStatus(HttpStatus.CREATED)
    Order createOrder(@RequestBody Order order, @AuthenticationPrincipal User user);

    @ApiOperation("Get all orders API")
    @ApiResponse(code = 200, message = "OK", response = Order.class)
    @GetMapping(value = "/orders")
    @ResponseStatus(HttpStatus.OK)
    List<OrderDto> getOrdersByUser(@AuthenticationPrincipal User user);

    @ApiOperation("Get order Id API")
    @ApiResponse(code = 200, message = "OK", response = Order.class)
    @GetMapping(value = "/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    OrderDto getOrderById(@PathVariable Long id);

    @ApiOperation("Update userOrders API")
    @ApiResponse(code = 201, message = "Update", response = Order.class)
    @PutMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    Order updateOrder(@RequestBody Order order);

    @ApiOperation("Delete userOrders API")
    @ApiResponse(code = 204, message = "Delete")
    @DeleteMapping(value = "/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<Void> deleteOrder(@AuthenticationPrincipal User user, @PathVariable Long id);

}
