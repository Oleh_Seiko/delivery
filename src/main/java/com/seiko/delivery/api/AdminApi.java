package com.seiko.delivery.api;

import com.seiko.delivery.controller.model.UserModel;
import com.seiko.delivery.dto.OrderDto;
import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Admin management REST API")
@RequestMapping("/api/v1/admin")
public interface AdminApi {

    @ApiOperation("Get list of all users")
    @ApiResponse(code = 200, message = "OK", response = CollectionModel.class)
    @GetMapping("/users")
    @ResponseStatus(HttpStatus.OK)
    CollectionModel<UserModel> getAllUsers();

    @ApiOperation("Get users by id")
    @ApiResponse(code = 200, message = "OK", response = UserModel.class)
    @GetMapping("/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    UserModel getUserById(@PathVariable Long id);

    @ApiOperation("Del users by id")
    @ApiResponse(code = 200, message = "OK", response = User.class)
    @DeleteMapping("/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<Void> deleteUserById(@AuthenticationPrincipal User user, @PathVariable Long id);

    @ApiOperation("Get list of all orders")
    @ApiResponse(code = 200, message = "OK", response = Order.class)
    @GetMapping("/orders")
    @ResponseStatus(HttpStatus.OK)
    List<OrderDto> getAllOrders();

    @ApiOperation("Get orders by id")
    @ApiResponse(code = 200, message = "OK", response = Order.class)
    @GetMapping("/orders/{id}")
    @ResponseStatus(HttpStatus.OK)
    OrderDto getOrderById(@PathVariable Long id);
}
