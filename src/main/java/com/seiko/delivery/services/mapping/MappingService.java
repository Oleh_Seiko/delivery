package com.seiko.delivery.services.mapping;

import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.User;

public interface MappingService {

    User mapFromUserDtoToUser(UserDto userDto);

    UserDto mapFromUserToUserDto(User user);

}
