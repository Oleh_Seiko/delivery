package com.seiko.delivery.services.impl;

import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.User;
import com.seiko.delivery.services.mapping.MappingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;

@Slf4j
@Service
public class MappingServiceImpl implements MappingService {

    @Override
    public User mapFromUserDtoToUser(UserDto userDto) {
        log.info("mapFromUserDtoToUser: method is called", userDto);
        User user = new User();
        try {
            BeanUtils.copyProperties(user, userDto);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        log.info("mapFromUserDtoToUser: method is called", user);

        return user;
    }

    @Override
    public UserDto mapFromUserToUserDto(User user) {
        log.info("mapFromUserToUserDto: from user: {}", user);
        UserDto userDto = new UserDto();
        try {
            BeanUtils.copyProperties(userDto, user);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        userDto.setPassword(null);
        log.info("mapFromUserToUserDto: mapped userDto: {}", userDto);
        return userDto;
    }
}
