package com.seiko.delivery.services.impl;

import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.exception.user.UnableToGetUserException;
import com.seiko.delivery.model.User;
import com.seiko.delivery.repository.UserRepository;
import com.seiko.delivery.services.UserService;
import com.seiko.delivery.services.mapping.MappingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final MappingService mappingService;

    @Override
    public UserDto getUserById(Long userId) {
        log.info("getUser started");
        User user;
        try {
            user = userRepository.findById(userId)
                    .orElseThrow(() -> new RuntimeException("User not found"));
            log.info("User with id {} was retrieved", user.getId());
        } catch (Exception ex) {
            log.error("Unable to get user with id {}", userId);
            throw new UnableToGetUserException("Unable to get user", ex);
        }
        return mappingService.mapFromUserToUserDto(user);
    }

    @Override
    public UserDto createUser(UserDto userDto) {
        log.info("createUser(): start");
        User user = mappingService.mapFromUserDtoToUser(userDto);
        log.info("createUser(): ", user);
        return mappingService.mapFromUserToUserDto(userRepository.save(user));
    }

    @Override
    public UserDto updateUser(UserDto userDto) {
        log.info("updateUser started");
        User user = mappingService.mapFromUserDtoToUser(userDto);
        log.info("updateUser: ", user);
        return mappingService.mapFromUserToUserDto(userRepository.save(user));
    }


    @Override
    public void deleteUser(User user) {
        log.info("deleteUser: with id {}", user.getEmail());
        SecurityContextHolder.clearContext();
        userRepository.delete(user);
        log.info("deleteUser: end method");
    }

    @Override
    public void deleteUserById(User user, Long id) {
        log.info("deleteUserById: start");
        List<User> allUsers = userRepository.findAll();
        log.info("allUsers: ", allUsers.size());
        for (User allUser : allUsers) {
            if (allUser.getId().equals(id)) {
                userRepository.deleteById(allUser.getId());
                ;
            }
        }
        log.info("deleteUserById: end method");

    }

    @Override
    public List<UserDto> getAllUsers() {
        log.info("getAllUsers(): method is called");
        List<User> allUsers = userRepository.findAll();
        log.info("getAllUsers: allUsers", allUsers);
        List<UserDto> userDto = allUsers
                .stream()
                .map(user -> mappingService.mapFromUserToUserDto(user))
                .collect(Collectors.toList());
        log.info("getAllUsers: userDto", userDto);
        log.info("getAllUsers: {} user found");
        return userDto;
    }

    @Override
    public boolean isUserExistsWithEmail(String email) {
        log.info("isUserExistsWithEmail: start");
        Optional<User> userByEmail = userRepository.findByEmail(email);
        log.info("isUserExistsWithEmail: ", userByEmail);
        return userByEmail.isPresent();
    }
}
