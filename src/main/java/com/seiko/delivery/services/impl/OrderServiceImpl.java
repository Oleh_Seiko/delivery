package com.seiko.delivery.services.impl;

import com.seiko.delivery.dto.OrderDto;
import com.seiko.delivery.exception.order.UnableToGetOrderException;
import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import com.seiko.delivery.repository.OrderRepository;
import com.seiko.delivery.services.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;


    @Override
    public List<OrderDto> getAllOrders() {
        log.info("getAllOrders: start");
        List<OrderDto> orderDtoList = new ArrayList<>();
        List<Order> orderList = orderRepository.findAll();
        log.info("getAllOrders: orderList {}", orderList);
        for (Order order : orderList) {
            log.info("getAllOrders: order {}", order);
            OrderDto orderDto = mapFromOrderToOrderDto(order);
            log.info("getAllOrders: orderDto {}", orderDto);
            orderDtoList.add(orderDto);
        }
        log.info("orderDtoList {}", orderDtoList);
        return orderDtoList;
    }

    @Override
    public List<OrderDto> getOrdersByUser(User user) {
        log.info("getOrdersByUser(User user, Long id): start");
        List<OrderDto> orders = new ArrayList<>();
        List<Order> allOrders = orderRepository.findByUser(user);
        log.info("allOrders: ", allOrders);
        for (Order allOrder : allOrders) {
            log.info("cycle for -> allOrder");
            OrderDto orderDto1 = mapFromOrderToOrderDto(allOrder);
            orderDto1.setUser(user);
            orders.add(orderDto1);

        }
        return orders;
    }

    @Override
    public OrderDto getOrderById(Long id) {
        log.info("getOrderById: is ok");
        Order order;
        try {
            order = orderRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("Order not found"));
            log.info("getOrder with id {} was retrieved", order.getId());
        } catch (Exception ex) {
            log.error("Unable to get order with id {}", id);
            throw new UnableToGetOrderException("Unable to get order", ex);
        }
        return mapFromOrderToOrderDto(order);
    }

    @Override
    public OrderDto createOrder(OrderDto orderDto) {
        log.info("createOrder is ok");
        Order order = mapFromOrderDtoToOrder(orderDto);
        order = orderRepository.save(order);
        log.info("created Order: id {}", order.getId());
        return mapFromOrderToOrderDto(order);
    }

    @Override
    public OrderDto updateOrder(OrderDto orderDto) {
        log.info("updateOrder: start");
        Order order = mapFromOrderDtoToOrder(orderDto);
        Order update = orderRepository.save(order);
        log.info("updateOrder: update ", update);
        return mapFromOrderToOrderDto(update);
    }

    @Override
    public void deleteOrderById(Long id) {
        log.info("deleteOrderById: start");
        orderRepository.deleteById(id);
        log.info("deleteOrderById: deleted Order by id");
    }

    private Order mapFromOrderDtoToOrder(OrderDto orderDto) {
        log.debug("mapOrderToOrderDto: start");
        Order order = new Order();
        try {
            BeanUtils.copyProperties(order, orderDto);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return order;
    }

    private OrderDto mapFromOrderToOrderDto(Order order) {
        log.info("mapFromOrderToOrderDto: start");
        OrderDto orderDto = new OrderDto();
        log.info("mapFromOrderToOrderDto: orderDto ", orderDto);
        try {
            BeanUtils.copyProperties(orderDto, order);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        log.debug("mapUserToUserDto: map from User to OrderDto", orderDto);
        return orderDto;
    }



    @Override
    public Order create(Order order) {
        log.info("create: method create it was created");
        Order created = orderRepository.save(order);
        log.info("create: created", created);
        return created;
    }

    @Override
    public void delete(User user, Long id) {
        log.info("delete: start" + user);
        List<Order> listOrders = orderRepository.findByUser(user);
        log.info("delete: listOrders", listOrders);
        for (Order order : listOrders) {
            log.info("delete: -> for ", order);
            if (order.getId().equals(id)){
                order.setId(id);
                orderRepository.delete(order);
            }
        }
        log.info("delete: Order by id");
    }


    @Override
    public Order update(Order order) {
        log.info("update: start");
        Order update = orderRepository.save(order);
        log.info("update: ", update);
        return update;
    }
}
