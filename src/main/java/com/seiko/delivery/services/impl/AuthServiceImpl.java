package com.seiko.delivery.services.impl;

import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.exception.user.UserNotFoundException;
import com.seiko.delivery.model.User;
import com.seiko.delivery.model.enums.Role;
import com.seiko.delivery.repository.UserRepository;
import com.seiko.delivery.services.auth.AuthService;
import com.seiko.delivery.services.mapping.MappingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {


    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final MappingService mappingService;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        log.info("loadUserByUsername: start");
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException("User to find user email"));
    }

    @Override
    public UserDto signIn(UserDto userDto) {
        log.info("signIn: start");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userDto.getEmail(),
                        userDto.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        User user = (User) authentication.getPrincipal();
        log.info("signIn: user", user);
        return mappingService.mapFromUserToUserDto(user);
    }

    @Override
    public UserDto signUp(UserDto userDto, Role role) {
        log.info("signUp: start");
        User user = mappingService.mapFromUserDtoToUser(userDto);
        log.info("createUser: about to create a new user with email {}", user.getEmail());

        user.setRole(role);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user = userRepository.save(user);

        log.info("User with id {} successfully registered", user.getId());
        return signIn(userDto);
    }

    @Override
    public void signOut() {
        log.info("signOut: start");
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info("Singing out user with email {}", user.getEmail());
        SecurityContextHolder.clearContext();
    }


}
