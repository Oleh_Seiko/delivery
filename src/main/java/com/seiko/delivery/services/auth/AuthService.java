package com.seiko.delivery.services.auth;

import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.enums.Role;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthService extends UserDetailsService {

    UserDto signIn(UserDto userDto);
    UserDto signUp(UserDto userDto, Role role);
    void signOut();
}
