package com.seiko.delivery.services;

import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;

import java.util.List;

public interface UserService {

    UserDto getUserById(Long id);
    UserDto createUser(UserDto userDto);
    UserDto updateUser(UserDto userDto);
    void deleteUser(User user);
    void deleteUserById(User user, Long id);
    List<UserDto>getAllUsers();
    boolean isUserExistsWithEmail(String email);


}
