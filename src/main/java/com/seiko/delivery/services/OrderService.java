package com.seiko.delivery.services;

import com.seiko.delivery.dto.OrderDto;
import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import org.aspectj.weaver.ast.Or;

import java.util.List;

public interface OrderService {
    List<OrderDto> getAllOrders();

    List<OrderDto> getOrdersByUser(User user);

    OrderDto getOrderById(Long id);

    OrderDto createOrder(OrderDto orderDto);

    OrderDto updateOrder(OrderDto orderDto);

    void deleteOrderById(Long id);

    Order create(Order order);

    Order update(Order order);

    void delete(User user, Long id);


}
