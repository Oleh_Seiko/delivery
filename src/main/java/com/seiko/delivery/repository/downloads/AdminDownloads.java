package com.seiko.delivery.repository.downloads;

import com.seiko.delivery.dto.UserDto;
import com.seiko.delivery.model.enums.Role;
import com.seiko.delivery.services.auth.AuthService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdminDownloads {

    @Bean
    public CommandLineRunner demoAdmin(AuthService authService,
    @Value("${app.auth.admin.password}") String password,
    @Value("${app.auth.admin.email}") String email) {
        return args -> {
            UserDto userDto = new UserDto();
            userDto.setName("Admin");
            userDto.setEmail(email);
            userDto.setPassword(password);
            authService.signUp(userDto, Role.ROLE_ADMIN);

            UserDto userDto2 = new UserDto();
            userDto2.setName("Kolya");
            userDto2.setSurname("Kolyan");
            userDto2.setEmail("k@k.com");
            userDto2.setPassword("k");
            authService.signUp(userDto2, Role.ROLE_USER);

            UserDto userDto3 = new UserDto();
            userDto3.setName("Adam");
            userDto3.setSurname("Adamovych");
            userDto3.setEmail("a@a.com");
            userDto3.setPassword("a");
            authService.signUp(userDto3, Role.ROLE_USER);

            UserDto userDto4 = new UserDto();
            userDto4.setName("Ted");
            userDto4.setSurname("Tedovich");
            userDto4.setEmail("t@t.com");
            userDto4.setPassword("t");
            authService.signUp(userDto4, Role.ROLE_USER);

            UserDto userDto5 = new UserDto();
            userDto5.setName("Bob");
            userDto5.setSurname("Bobovich");
            userDto5.setEmail("b@b.com");
            userDto5.setPassword("b");
            authService.signUp(userDto5, Role.ROLE_USER);
        };
    }
}
