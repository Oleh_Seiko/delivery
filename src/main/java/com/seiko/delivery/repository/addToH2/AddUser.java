package com.seiko.delivery.repository.addToH2;

import com.seiko.delivery.model.User;
import com.seiko.delivery.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

//@Slf4j
//@Component
//public class AddUser {
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @PostConstruct
//    public void setUser() {
//        log.info("Created method findAllUsers() to add H2");
//        User user1 = new User();
//        user1.setName("Kokos");
//        user1.setSurname("Kokosov");
////        user1.setPassword("k");
//        user1.setEmail("k@k.com");
//
//        User user2 = new User();
//        user2.setName("Abrikos");
//        user2.setSurname("Abrikosov");
////        user2.setPassword("a");
//        user2.setEmail("a@a.cpm");
//
//        User user3 = new User();
//        user3.setName("Tomat");
//        user3.setSurname("Tomatov");
////        user3.setPassword("t");
//        user3.setEmail("t@t.cpm");
//
//        User user4 = new User();
//        user4.setName("Banan");
//        user4.setSurname("Bananov");
////        user4.setPassword("b");
//        user4.setEmail("b@b.cpm");
//
//        User user5 = new User();
//        user5.setName("Kivi");
//        user5.setSurname("Kivin");
////        user5.setPassword("k");
//        user5.setEmail("c@c.cpm");
//
//        userRepository.save(user1);
//        userRepository.save(user2);
//        userRepository.save(user3);
//        userRepository.save(user4);
//        userRepository.save(user5);
//    }
//}
