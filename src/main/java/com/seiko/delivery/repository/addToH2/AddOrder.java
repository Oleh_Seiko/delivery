package com.seiko.delivery.repository.addToH2;

import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import com.seiko.delivery.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

//@Slf4j
//@Component
//public class AddOrder {
//
//    @Autowired
//    private OrderRepository orderRepository;
//
//    @PostConstruct
//    public void setOrder(){
//        log.info("Created method findAllOrders() to add H2");
//
//        Order order1 = new Order();
//        order1.setCityStart("Lviv");
//        order1.setCityFinish("Lviv");
//        order1.setWeight(12);
//        order1.setKm(70);
//        order1.setSum(1200);
//        order1.setDate("2021-11-05");
//        order1.setUser(order1.getUser());
////        order1.setUserId("1");
//
//        Order order2 = new Order();
//        order2.setCityStart("Lviv");
//        order2.setCityFinish("Rivne");
//        order2.setWeight(20);
//        order2.setKm(250);
//        order2.setSum(4500);
//        order2.setDate("2021-11-05");
//        order2.setUser(new User());
////        order2.setUserId("2");
//
//        Order order3 = new Order();
//        order3.setCityStart("Lviv");
//        order3.setCityFinish("Kiev");
//        order3.setWeight(6);
//        order3.setKm(560);
//        order3.setSum(6500);
//        order3.setDate("2021-11-04");
//        order3.setUser(new User());
////        order3.setUserId("3");
//
//        Order order4 = new Order();
//        order4.setCityStart("Kiev");
//        order4.setCityFinish("Kiev");
//        order4.setWeight(5);
//        order4.setKm(120);
//        order4.setSum(2500);
//        order4.setDate("2021-11-04");
//        order4.setUser(new User());
////        order4.setUserId("4");
//
//        Order order5 = new Order();
//        order5.setCityStart("Kiev");
//        order5.setCityFinish("Lviv");
//        order5.setWeight(10);
//        order5.setKm(600);
//        order5.setSum(8000);
//        order5.setDate("2021-11-06");
//        order5.setUser(new User());
////        order5.setUserId("5");
//
//        orderRepository.save(order1);
//        orderRepository.save(order2);
//        orderRepository.save(order3);
//        orderRepository.save(order4);
//        orderRepository.save(order5);
//    }
//}
