package com.seiko.delivery.repository;

import com.seiko.delivery.model.Order;
import com.seiko.delivery.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Order save(Order order);
    void deleteById(User user);
    List<Order> findByUser(User user);
}
